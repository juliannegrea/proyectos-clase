#ifndef _VECTOR_H_
#define _VECTOR_H_


class vector
{
public:		//atributos publicos no
	double x;
	double y;
	double z;

	//definimos Metodo Constructor mismo nombre clase no return
	vector(double x=0, double y=0 double z=0);

	//definimos operador miembro con argumento referencia constante 
	vector operator + (const vector &op);
	// el primer operador vector es argumento implicito y se accede a el mediante el punto
	//pero el segundo vector si necesita ser pasado por referencia en vector.cpp



};

#endif