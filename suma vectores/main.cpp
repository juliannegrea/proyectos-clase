#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

//creamos funcion imrimir vector

void imprimir(vector v){
	printf("(%i, %i, %i"), v.x, v.y, v.z);
}

int main(int argc, char const *argv[])
{
	vector v0 = vector(2, 5, 3);
	vector v1 = vector(3, 2, 5);
	resultado = v0 + v1;

	imprimir(v0);
	printf(" + ");
	imprimir(v1);
	printf("=");
	imprimir(resultado);
	printf("\n");

	return 0;
}