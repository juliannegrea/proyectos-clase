#include <iostream>
#include <string>
#include <vector>
#include <ostream>


using namespace std;

int main(int argc, char const *argv[])
{
	std::vector<int> vec;

	int enteros;

	std::cout<<"Cuantos enteros quieres introducir: ";
	std::cin>>enteros;





	/****************************/
	/*FORMA NORMAL DE C ITERACION*/
	/***************************/


	for (int i = 0; i < vec.size(); ++i)						////función miembro size(), que devuelve el número de elementos que contiene///
	{

		int numero;
		std::cout<<"Intro un numero: ";
		std::cin>>numero;

		std::cout<<vec[i]<<std::endl;

		vec.push_back(numero);									//esta función recibe una referencia del valor de tipo de dato del vector que queremos añadir al contenedor//

	}


	/****************************/
	/*FORMA NORMAL DE C++ ITERACION*/
	/***************************/


	for (std::vector<int>::iterator it = vec.begin(); it != end(); ++it)
	{
		std::cout<<*it<<std::endl;

	}




	/****************************/
	/*FORMA NORMAL AUTO DE ITERACION*/
	/***************************/



	for (auto it = vec.begin(); it != end(); ++it)				//es mejor que sea el compilador el que deduzca automáticamente el tipo de dato
																//o el valor devuelto por una función en el momento de definir inicializandolo con una variable
																//una variable.
	{
		std::cout<<*it<<std::endl;
	}


	/****************************/
	/*FORMA  DE ITERACION*/
	/***************************/



	for (int elemento:vec)
	{
		std::cout<<elemento<<std::endl;
	}








	return 0;
}