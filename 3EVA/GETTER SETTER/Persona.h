#ifndef PERSONA_H
#define PERSONA_H

#include <string>
#include <iostream>
class Persona                                     //Declaramos la clase con el nombre Persona
{



private:                                          //a partir de aquí todos los miembros serán privados
                                                  //los datos miembro pueden ser cualquier tipo de dato, incluso otras clases como string
          std:: string nombre;
          int   edad;
          float estatura;
          float peso;


          float aumentaEstatura(float metros){return estatura += metros}; //función inline
          float aumentaPeso(float kilogramos){return peso += kilogramos};

        public:


//constructor
          Persona (const std::string& nombre,int edad, float peso, float estatura);
          Persona::~Persona();

          void saluda();
          int  cumpleEdad();


//setters
          void SetNombre(const string &name);
          void SetEdad(int year);
          void SetEstatura(float cm);
          void SetPeso(float kg);


//getters

          int  get_edad();
  std::string  get_nombre();
        float  get_estatura();
        float  get_peso():


};
#endif