#include "Persona.h"
#include <iostream>
using namespace std;






Persona::Persona(const string& nombre,int edad, float peso, float estatura){
  this -> nombre = nombre;
  this -> edad = edad;
  this -> peso = peso;
  this -> estatura = estatura;
}

Persona::~Persona(){}

//setters
  Persona::void SetNombre(const string& name)
  {

    nombre = name;

  }


  Persona::void SetEdad(int year)
  {
    edad = year;
  }


  Persona::void SetEstatura(float cm)
  {
    estatura = cm;
  }


  Persona::void SetPeso(float kg)
  {
    peso = kg;
  }


  //getters


  Persona::int  get_edad(){return edad};
  Persona::std::string  get_nombre(){return nombre}
  Persona::float  get_estatura(){return estatura}
  Persona::float  get_peso(){return peso}

void Persona::saluda(){
  cout << "¡Hola! me llamo " << this->nombre
       << ", tengo " << this->edad << " años"
       << ", peso " << this->peso << " kilos"
       << " y mido " << this->estatura << " metros" << endl;
}


// Cuando cumple edad cambia el peso edad y estatura en 0.1 por año que pasa hasta 20

int Persona::cumpleEdad(){
  float aumento_peso = 0;
  float aumento_estatura = 0;

  if (edad <= 20){
    aumento_peso = 0.1;
    aumento_estatura = 0.1;
  }

  aumentaPeso(aumento_peso);
  aumentaEstatura(aumento_estatura);
  edad += 1;
  return edad;
}