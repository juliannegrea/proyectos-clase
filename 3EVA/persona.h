#include <iostream>
#include <stdlib.h>
#include <string>

#ifndef PERSONA_H
#define PERSONA_H



class Persona{						//Nombre de la clase

									//Miembros privadods
private:

				int			 edad;
				std::string  nombre;

public:								//Metodos publicos

	Persona(const std::string& nombre,int edad);
	void leer();
	void correr();

};



#endif
