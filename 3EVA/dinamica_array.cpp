#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    
    /*DECLARAMOS LAS VARIABLES QUE VAMOS A USAR */


int i;
int tam;
int *vec;

    /*RECOOJO LOS DATOS INTRODUCIDOS*/

    printf("INTRODUCE EL TAMAÑO DEL VECTOR:");
        scanf("%i", &tam);

    /*RESERVAMOS MEMORIA PARA EL VENCTOR*/

    vec = (int*)malloc(tam*sizeof(int));

    if (vec==NULL)
    {
        printf("!!NO RESERVA MEMORIA!!\n");
    }


    /*RELLENO ARRAY*/

    for (int i = 0; i < tam; i++)
    {
        printf ("v[%n] = ", vec+1);
        scanf (" %i", vec + i);

    }

    /*IMPRIMO ARRAY*/

    for (int i = 0; i < tam; i++){
        printf("vec[%i]\n", vec[i] );
    }    

    printf("\n");

    free(vec);
    return 0;
}