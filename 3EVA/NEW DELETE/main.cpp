#include<stdio.h>
#include<iostream>




using namespace std;

int main(int argc, char const *argv[])
{

/*OPERADORES NEW-> LO QUE HACE ES RESERVA DINAMICA DE MEMORIA PARA UN OBJETO
  PERADOR DELETE-> SIMILAR A FREE LIBERA MEMORIA CREADA POR UNN VECTOR 	  */

	Cuenta *c1 = new Cuenta;		/*crea puntero que apunta a la direccion de memoria del objeto c1
									  con new se reserva la memoria para el bojeto de la clase Cuenta*/
	delete c1;

	Cuenta *cuentas = new Cuenta[100];  /*se crea un vector que guarda 100 cuentas con memoria dinamica*/
	delete [] cuentas;					/*libera la memoria */
	return 0;
}