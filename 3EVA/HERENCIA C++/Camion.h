
#ifndef CAMION_H
#define CAMION_H

#include "VehiculoRodante.h"


//para que la clase Camion pueda heredar los campos de la clase base VehiculoRodante esta se tiene que declarar como public
class Camion : public VehiculoRodante
{
        public:


          Camion();
  virtual ~Camion();

          void set_carga();
          int get_carga();

          void Mostrar();

        private:


          int mCarga;



#endif // CAMION_H
