#ifndef _Cuenta_
#define _Cuenta_

#include <iostream>



class Cuenta
{
			protected:			//para que las clases derivadas puedan acceder a los datos de la clase base que es Cuenta

				char  *Nombre;
				double Saldo;
				double Interes;
			public:
				Cuenta (const char *unNombre,double unSaldo, double unInteres);			//constructor sirve para inicializar atributos de la clase
																						//para poder crear un objeto y asignarle los valores y para
																						//que no exista miembros sin valor que son basura


				//getters(acceder al valor)
				char   *GetNobre();
				double  GetSaldo(){return Saldo}
				double  GetInteres(){return Interes}

				//setters(dar valor)

				void SetNombre(unNombre){
					Nombre = unNombre;
				}

				void SetSaldo(unSaldo){
					Saldo = unSaldo;
				}

				void SetInteres(unInteres){
					Interes = unInteres;
				}

				//metodo 

				void Ingreso(double);

				//sobrecarga operator <<
				friend ostream& operator<<(ostream& out, Cuenta& unaCuenta);



				~Cuenta();

};


#endif