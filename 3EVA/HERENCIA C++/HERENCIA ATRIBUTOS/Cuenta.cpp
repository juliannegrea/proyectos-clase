#include "Cuenta.h"
#include <string.h>


using namespace std;

/*FORMA NORMAL CON PARAMETROS INICALIZAR*/

	Cuenta::Cuenta(const char *unNombre,double unSaldo,double unInteres)
	{

		Nombre   = new char[strlen(unNombre)+1];			//reserva memoria dinamica de cada letra del con new
															//strlen saca la longitud de la cadena byte a byte
		strcpy(Nombre,unNombre);
		Saldo   = unSaldo;
		Interes = unInteres;
	}


	~Cuenta(){
		delete [] Nombre;									//liberamos la memoria
	}


//getters

		double GetSaldo(){return Saldo};
		double GetInteres(){return Interes};


//setters

		void SetSaldo(unSaldo){
			Saldo = unSaldo;
		}

		void SetInteres(unInteres){
			Interes = unInteres;
		}


		void Ingreso(double unaCantidad){
			SetSaldo(GetSaldo() + unaCantidad) 				//ponemos un valor al saldo,accedemos a la cantidad y le sumamos unaCantiad
															//que vaya a ingresar la persona.(lo que tenia mas lo que ingresa)
		}


		friend ostream& operator <<(ostream& out, Cuenta& unaCuenta){

			out<<"Nombre: "<<unaCuenta.GetNombre()<<endl;
			out<<"Dinero en la Cuenta: "<<unaCuenta.GetSaldo()<<endl;

			return out;
		}

