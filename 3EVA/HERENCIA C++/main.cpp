#include <stdio.h>
#include <stdlib.h>


#include <iostream>



using namespace std;


int main(int argc, char const *argv[])
{


	//INSTANCIAR UNA CLASE ::crear un objeto,
	//Creamos dos objetos de la clase derivada Camion1 y Camion2.

	  Camion Camion1;
	  Camion Camion2;


	  Camion1.set_ruedas(4);
	  Camion1.set_pasajeros(5);
	  Camion1.set_carga(3200);

	  Camion2.set_ruedas(6);
	  Camion2.set_pasajeros(3);
	  Camion2.set_carga(1200);



	  Camion1.Mostrar();
  	  std::cout << std::endl;

      Camion2.Mostrar();
      std::cout << std::endl;

	return 0;
}