
#ifndef VEHICULORODANTE_H
#define VEHICULORODANTE_H


/**
  * class VehiculoRodante
  *
  */

class VehiculoRodante

{

public:
  // Constructors/Destructors
  //
         VehiculoRodante();
virtual ~VehiculoRodante();

      //Getters /sirve para mostrar el valor de la variable
      int get_ruedas();
      int get_pasajeros()
      //Setter /sirve para asignacion del valor
      void set_ruedas();
      void set_pasajeros();

private:

      //metodos privados de la clase que van a ser heredados
      int mRuedas;
      int mPasajeros;


};
#endif // VEHICULORODANTE_H
