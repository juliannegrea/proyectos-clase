#ifndef  _vector_
#define  _vector_

class vector
{
public:

	double x;
	double y;
	double z;
	vector(double x=0,double y=0,double z=0);

	//sobrecarga del operador + suma se carga como un metodo mas de la clase
	//para que asi podemos usarlo para sumar objetos

	//nombreclase operator+(const nombreclase &nombrereefrencia)
	vector operator+(const vector &op);

	~vector();

};



#endif