
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	

int *pt; 						/*declaramos el puntero con de tipo entero*/

pt = (int *) malloc(4);				/*usamos malloc para reservar una catidad de memoria para reservar un entero */

*pt= 10;							/*le decimos el entero para el que queremos guardar memoria*/


  printf ("%p: %i\n", pt, *pt);		/*%p imprime el valor del puntero , imprime el in 10 en este caso 
  										pt, *pt */

      free (pt);					/*se tiene que poner siempre para liberar el espacio */

	return 0;
}