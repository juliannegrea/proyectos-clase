tres-v5: Comprobar el 4 en raya vertical

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "general.h" /*a un archivo punto h nos podemos guardar todo aquello que no genere programa 
                        puede estar def tipo dato todos los #define y las declaracionies de funciones 
                        pero no las definiciones*/



const char *representa[] = {
    "  ",
    "⭕", // 2B55
    "❌", // 274C
};

void pintar (unsigned t[N][N]){
    printf ("\n");
    for (unsigned f=0; f<N; f++) {
        printf ("\t");
        for (unsigned c=0; c<N; c++){
            printf (" %s ", representa[t[f][c]]);
            if (c < N-1)
                printf ("│");
        }
        printf ("\n");

        // Imprimir una linea horizontal
        printf ("\t");
        if (f < N-1)
            for (unsigned c=0; c<N; c++) {
                printf ("────");
                if (c < N-1)
                    printf ("┼");
            }
        printf ("\n");
    }

    printf ("\n");
}

void fingir_datos(unsigned t[N][N]) {
    t[2][0] = t[1][0] = t[0][0] = j1;

    t[1][2] = j2;
}

bool check_hztal(unsigned t[N][N], unsigned fila, enum TDato jugador) {
    unsigned contador = 0U;

    if (fila >= N ){
        fprintf(stderr, "check_hztal: Invalid row %u.\n", fila);
        exit( OUT_OF_BOUNDS );
    }

    for (unsigned i=0; i<N; i++)
        if (t[fila][i] == jugador)
            contador++;

    return contador == N;
}

bool check_vtcal(unsigned t[N][N], unsigned col, enum TDato jugador) {
    unsigned contador = 0U;

    if (col >= N ){
        fprintf(stderr, "check_hztal: Invalid col %u.\n", col);
        exit( OUT_OF_BOUNDS );
    }

    for (unsigned i=0; i<N; i++)
        if (t[i][col] == jugador)
            contador++;

    return contador == N;
}


int main (int argc, char *argv[]) {

    unsigned tablero[N][N];

    /* Inicialización */
    bzero (tablero, sizeof(tablero));
    fingir_datos(tablero);

    for (unsigned c=0U; c<N; c++)
        if (check_vtcal (tablero, c, j1))
            printf ("Tres en raya en la columna: %u!\n", c);
        else
            printf ("Columna %u: Libre.\n", c);

    pintar(tablero);

    return EXIT_SUCCESS;
}
